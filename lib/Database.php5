<?

class Database
{// Classe : in�cio

 	public	$host;      //Nome do servidor
 	public	$DBName;     //Banco MySQL
 	public	$usuario;   //Usuario MySQL
 	public	$senha;     //Senha MySQL
	public	$query;
	public	$result;
	public	$array;
	public	$object;
	public	$rows;
	public	$lastInsertedId;
	public	$now;
	public  $indiceLink;

	public static $arrLinks = array();
	public static $arrDBNames = array();
	public static $arrAutocommit = array();

	public function __construct($databaseName = false)
	{// M�todo : in�cio - Construtor

	    if($databaseName)
	        $this->DBName = $databaseName;
	    else
	    	$this->DBName = "projetos";

		$this->host		=	"localhost";
		$this->porta	=	"3306";
		$this->usuario	=	"root";
		$this->senha	=	"";

		$this->lastInsertedId = "";
	 	$this->rows = 0;
	 	$this->now	= date("Y-m-d H:i:s");

	 	$this->indiceLink = -1;

	}// M�todo : Fim - Construtor

	public function iniciarTransacao(){

	    mysqli_autocommit(self::$arrLinks[$this->indiceLink], false);
	    mysqli_query(self::$arrLinks[$this->indiceLink], "START TRANSACTION;");

	    self::$arrAutocommit[$this->indiceLink] = false;

	}

	public function commitTransacao(){

	    if(self::$arrAutocommit[$this->indiceLink] === false){

	        mysqli_query(self::$arrLinks[$this->indiceLink], "COMMIT;");

	    }

	}

	public function rollbackTransacao(){

	    if(self::$arrAutocommit[$this->indiceLink] === false){

	        mysqli_query(self::$arrLinks[$this->indiceLink], "ROLLBACK;");

	    }

	}

	public function getUser(){

	    return $this->usuario;

	}

	public function getSenha(){

	    return $this->senha;

	}

	public function getHost(){

	    return $this->host;

	}

	public function getDBName(){

	    return $this->DBName;

	}

	public function __destruct(){



	}


	public function OpenLink()
	{// M�todo : in�cio

	    if(!in_array($this->DBName, self::$arrDBNames)){

	        $indiceLink = count(self::$arrLinks);
	        $this->indiceLink = $indiceLink;

	        self::$arrDBNames[$this->indiceLink] = $this->DBName;
            self::$arrAutocommit[$this->indiceLink] = true;

	        self::$arrLinks[$this->indiceLink] = @mysqli_connect($this->host, $this->usuario, $this->senha, $this->DBName)

    		or die (

            		print "
            			<div class='divErro'>
            				<table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'>
            					<tr>
            						<td class='imgErro' valign='top'>&nbsp;</td>
            						<td class='conteudosDiv'>
            							<b>CONEX�O COM O BANCO DE DADOS N�O PODE SER ESTABELECIDA</b>
            						</td>
            					</tr>
            				</table>
            			</div>"
        			);

			$this->SelectDB();

	    }
	    else{

	        $this->indiceLink = array_search($this->DBName, self::$arrDBNames);

    	    if(self::$arrLinks[$this->indiceLink] && mysqli_errno(self::$arrLinks[$this->indiceLink])){

    	        //se tem 2 links com o mesmo db, eh pq deu erro, se tiver mais
    	        //eh pq deu erro dentro do erro, ocasionando um loop infinito
    	        //entao para

    	        if(count(array_keys(self::$arrDBNames, $this->DBName)) > 2){

    	            exit();

    	        }

                $this->indiceLink = count(self::$arrLinks);

                self::$arrLinks[$this->indiceLink] = @mysqli_connect($this->host, $this->usuario, $this->senha, $this->DBName);
                self::$arrDBNames[$this->indiceLink] = $this->DBName;

                $this->SelectDB();

            }

	    }

	}// M�todo : fim


	private function SelectDB()
	{// M�todo : in�cio

	    if($this->indiceLink == -1){

	        $this->OpenLink();

	    }

		mysqli_select_db(self::$arrLinks[$this->indiceLink], self::$arrDBNames[$this->indiceLink])

		or die(

			print "
				<div class='divErro'>
					<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='imgErro' valign='top'>&nbsp;</td>
							<td class='conteudosDiv'>
								<b>ADODB: ERRO AO SELECIONAR O BANCO DE DADOS</b>
								<br/><br/>
								<b>N� DO ERRO: </b> ". mysqli_errno(self::$arrLinks[$this->indiceLink])."
								<br/><br/>
								<b>ERRO:</b> ". mysqli_error(self::$arrLinks[$this->indiceLink])."
							</td>
						</tr>
					</table>
				</div>"
		);

	}// M�todo : fim

	public function imprimirQuery($formatacao = false){

		if($formatacao){

			print($this->query);

		}
		else{

			printNaoFormatada($this->query . "<br /><br />");

		}

	}

	public function query($query)
	{// M�todo : in�cio

	    if($this->indiceLink == -1){

	        $this->OpenLink();
	        $this->SelectDB();

	    }

		$this->query = $query;

	 	$this->result =	mysqli_query(self::$arrLinks[$this->indiceLink], $this->query);

	 	if(!$this->result)
	 	{

            if(substr(strtolower($query), 0 , 6) == "insert"){

                $codigosDeErro = array(1062);

		        if(in_array(mysqli_errno(self::$arrLinks[$this->indiceLink]), $codigosDeErro)){

		        	$this->imprimirQuery(true);
		            print("O Registro n�o p�de ser inserido pois j� existe um registro com a mesma chave �nica");
    	 		    $this->rollbackTransacao();
		            exit();

		        }

            }
            elseif(substr(strtolower($query), 0 , 6) == "delete"){

                $codigosDeErro = array(1216, 1217, 1451, 1452);

		        if(in_array(mysqli_errno(self::$arrLinks[$this->indiceLink]), $codigosDeErro)){

                    print("O Registro n�o p�de ser removido devido � depend�ncias com outras entidades");
         		    $this->rollbackTransacao();
                    exit();

    	 		}

            }

            $errno = mysqli_errno(self::$arrLinks[$this->indiceLink]);
            $errstr = urlencode(mysqli_error(self::$arrLinks[$this->indiceLink]));

            print("<b>ERRO NA EXECU��O DO COMANDO SQL</b>

		    						 <b>QUERY: </b> ". nl2br($this->query) . "
		    						 <b>N� DO ERRO: </b> " . mysqli_errno(self::$arrLinks[$this->indiceLink]) . "
		    						 <b>ERRO: </b> " . mysqli_error(self::$arrLinks[$this->indiceLink]). "
		    						 <b>BACKTRACE: </b>");

            $this->rollbackTransacao();

            exit();

	 	}
	 	else
	 	{

	 		if(substr(strtolower(str_replace("(", "", $query)), 0 , 6) == "select" || substr(strtolower($query), 0 , 4) == "show")
			{

		 		$this->rows 			=	mysqli_num_rows($this->result);
		 		return $this->result;

			}
			elseif(substr(strtolower($query), 0 , 6) == "insert")
			{
				$sql					=	"SELECT last_insert_id()";
				$con					=	mysqli_query(self::$arrLinks[$this->indiceLink], $sql);
				if(!$con)
				{

				    $this->rollbackTransacao();

				}
				else
				{

					$rs					  =	mysqli_fetch_object($con);
					$this->lastInsertedId =	$rs->Id;
					return $this->result;

				}
			}
			else
			{

				return $this->result;

			}
	 	}

	}// M�todo : fim

	public function voltarPonteiroDoResultSet(){
		
		return mysqli_data_seek($this->result);
		
	}

	public function fetchArray($constante=MYSQL_BOTH)
	{//M�todo : in�cio

		$this->array = mysqli_fetch_array($this->result, $constante);

		return $this->array;

	}//M�todo : fim


	public function fetchObject()
	{//M�todo : in�cio

		$this->object = mysqli_fetch_object($this->result);

		return $this->object;

	}//M�todo : fim


	public function getPrimeiraTuplaDoResultSet($campo)
	{//M�todo : in�cio

		return self::mysqli_result($this->result, 0, $campo);

	}//M�todo : fim

	public function getResultSet(){

	    return $this->result;

	}

	public function resultSet($linha, $coluna){

	    return Database::mysqli_result($this->result, $linha, $coluna);

	}

	public static function mysqli_result($resultSet, $linha, $coluna){

	    if(mysqli_data_seek($resultSet, $linha)){

	        $linha = $resultSet->fetch_array(MYSQLI_BOTH);

	        return $linha[$coluna];

	    }

	}
	
    public function getListaDosNomesDasTabelas() {


        // Pega todo pedido no status requisitado, seje relativo a um produto individual, ou a um combo, ou promocao.
        $this->Query("SHOW TABLES;");
        $v_listaNomeTabela = Helper::getResultSetToArrayDeUmCampo($this->result);
        $v_arrayRetorno = array();
        $v_index = 0;
        foreach ($v_listaNomeTabela as $v_strTabela) {
            $v_arrayRetorno[$v_index] = trim(strtolower($v_strTabela));
            $v_index += 1;
        }
           
        return $v_arrayRetorno;
    }

} // Class : fim

?>