<?

class Helper{
    
    public function __construct(){
        
        
        
    }
    
    public static function conferirPermissaoSobreEscrita($arquivo){
        
        if(!file_exists($arquivo))
            return true;
        
        $handleArquivo = fopen($arquivo, "r");
        
        $tamanhoLido = 1024;
        
        if(filesize($arquivo) < $tamanhoLido)
            $tamanhoLido = filesize($arquivo);
        
        $buffer = fread($handleArquivo, $tamanhoLido);
        
        if(substr_count($buffer, "@@NAO_MODIFICAR") > 0)
            return false; //para desabilitar verificacao, coloque 'return true';
            
        return true;
        
    }
    
    public static function GET($variavel){
        
        return ($_GET[$variavel]);
        
    }
    
    public static function POST($variavel){
        
        return ($_POST[$variavel]);
        
    }
    
    public static function SESSION($variavel){
        
        return ($_SESSION[$variavel]);
        
    }
    
    public static function includePHP($niveisRaiz, $caminhoApartirRaiz){
        
        $strRaiz = self::strNiveisRaiz($niveisRaiz);
        
        include_once($strRaiz . $caminhoApartirRaiz);
        
    }
    
    public static function carregarJavascript($niveisRaiz, $diretorioApartirRaiz, $arquivoSemExtensao){
        
        $strRaiz = self::strNiveisRaiz($niveisRaiz);
        
        $diretorioApartirRaiz = self::pathComBarra($diretorioApartirRaiz);
        
        $strPrint = "<script type=\"text/javascript\" src=\"{$strRaiz}{$diretorioApartirRaiz}{$arquivoSemExtensao}.js\" ></script>\n";        
        
        return $strPrint;
                
    }
    
    public static function carregarCss($niveisRaiz, $diretorioApartirRaiz, $arquivoSemExtensao){
        
        $strRaiz = self::strNiveisRaiz($niveisRaiz);
        
        $diretorioApartirRaiz = self::pathComBarra($diretorioApartirRaiz);
        
        $strPrint = "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$strRaiz}{$diretorioApartirRaiz}{$arquivoSemExtensao}.css\" />\n";        
        
        return $strPrint;
                
    }
    
    public static function pathComBarra($path){
        
        if(!(substr($path, strlen($path)-1, 1) == "/")){
            
            $path = $path . "/";
            
        }
                
        return $path;
        
    }
    
    public static function strNiveisRaiz($int){
        
        $strRetorno = "";
        
        for($i=0; $i<$int; $i++){
            
            $strRetorno .= "../";
            
        }
        
        return $strRetorno;
        
        
    }
    
    public static function getBarraDaNextAction($arr){

        $strRetorno = "\t<table class=\"table_next_action\">";
        $strRetorno .= "\t\t<tr class=\"tr_next_action\">";
        
        $i=0;
        
        foreach ($arr as $chave=>$valor){

            if(count($arr) == 1){
            
                $strRetorno .= "\t\t\t<td class=\"td_next_action\"><input type=\"hidden\" name=\"next_action\" value=\"{$chave}\" ></td>";
                break;
            
            }
            
            $checked = ($i==0)?"checked=\"checked\"":"";
            
            $strRetorno .= "\t\t\t<td class=\"td_next_action\"><input type=\"radio\" name=\"next_action\" $checked value=\"{$chave}\" />{$valor}</td>";   

            $i++;
            
        }
        
        $strRetorno .= "\t\t</tr>";
        $strRetorno .= "\t</table>";
        
        return $strRetorno;
                
    }
    
    public static function getBarraDeBotoesDoFormulario($botaoLimpar=true, $botaoCadastrar=true){

        $strRetorno = "\t<table class=\"table_botoes_form\">";
        $strRetorno .= "\t\t<tr class=\"tr_botoes_form\">";
        $strRetorno .= "\t\t\t<td class=\"td_botoes_form\">";
        
        if($botaoLimpar){
            
            $strRetorno .= "<input class=\"botoes_form\" type=\"reset\" value=\"Limpar\">";
                        
        }
        
        if($botaoCadastrar){
            
            $strRetorno .= "<input class=\"botoes_form\" type=\"submit\" value=\"Cadastrar\">";
                        
        }
        
        
        $strRetorno .= "\t\t\t</td>";
        $strRetorno .= "\t\t</tr>";
        $strRetorno .= "\t</table>";
        
        return $strRetorno;
                
    }
    
    public static function urlAction($actionPar, $id=""){
        
        $action = substr($actionPar, 0, strpos($actionPar, "_"));
        $page = substr($actionPar, strpos($actionPar, "_") + 1);
        
        if($action == "add"){
            
            $strRet = "index.php5?tipo=forms&page=$page";
            
        }
        elseif($action == "edit"){
            
            $strRet = "index.php5?tipo=forms&page=$page&id=$id";
            
        }
        elseif($action == "list"){
            
            $strRet = "index.php5?tipo=lists&page=$page";
            
        }
       
        return $strRet;
        
    }
    
    public static function verificarUploadArquivo($nomeCampo){
        
        if($_FILES[$nomeCampo]["tmp_name"] != ""){
            
            return true;
            
        }
        
        return false;
        
        
    }
    
    public static function getTipoCampo($campo){
        
        if(strpos($campo, "_DATETIME") !== false){

            return "DATETIME";
            
        }
        elseif(strpos($campo, "_DATE") !== false){

            return "DATE";
            
        }
        elseif(strpos($campo, "_FLOAT") !== false){

            return "FLOAT";
            
        }
        elseif(strpos($campo, "_INT") !== false){

            return "INT";
            
        }
        elseif(strpos($campo, "_BOOLEAN") !== false){

            return "BOOLEAN";
            
        }
        elseif(strpos($campo, "_IMAGEM") !== false){

            return "IMAGEM";
            
        }
        elseif(strpos($campo, "_ARQUIVO") !== false){

            return "ARQUIVO";
            
        }
        elseif(strpos($campo, "cpf") !== false){

            return "CPF";
            
        }
        elseif(strpos($campo, "cnpj") !== false){

            return "CNPJ";
            
        }
        elseif(strpos($campo, "cep") !== false){

            return "CEP";
            
        }
        elseif(strpos($campo, "email") !== false){

            return "EMAIL";
            
        }
        elseif(strpos(strtolower($campo), "telefone") !== false || strpos(strtolower($campo), "celular"
              || substr(strtolower($campo), strlen($campo)-3, strlen($campo)) == "tel")){

            return "TELEFONE";
            
        }
        else{
            
            return "TEXTO";
            
        }
        
        
    }
    
    public static function removerOsUltimosCaracteresDaString($string, $numeroCaracteres=1){

        return substr($string, 0, strlen($string)-($numeroCaracteres));

    }

    public static function removerOsPrimeirosCaracteresDaString($string, $numeroCaracteres=1){

        return substr($string, $numeroCaracteres, strlen($string)-($numeroCaracteres));

    }
    
    public static function getResultSetToArrayDeUmCampo($resultSet) {

        if (mysqli_num_rows($resultSet) > 0) {

            mysqli_data_seek($resultSet, 0);
        }

        $arrayRetorno = array();

        for ($i = 0; $registro = mysqli_fetch_array($resultSet); $i++) {

            $indice = $i;

            $arrayRetorno[$indice] = $registro[0];
        }

        return $arrayRetorno;
    }
}


?>