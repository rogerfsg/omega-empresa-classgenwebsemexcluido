<?

function gerarFiltro($table, $class, $key, $label, $ext, $sobrescrever, $arrCampos){

    $database = new Database();

    $objBanco = new Database();

    $idProjeto = $_POST["projeto"];

    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioFiltros, p.colunasForms_INT FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));
    
    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);
    $colunas = Database::mysqli_result($objBanco->result, 0, 5);

    $database->OpenLink();
    
    $dir = dirname(__FILE__);
    $filedate = date("d.m.Y");

    $filename = $dir . "/../../" . $diretorio ."/" . $table . ".php5";

    $sql = "SHOW TABLES LIKE '$table';";
    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $file = fopen($filename, "w+");

        $genero   = $_POST["genero_entidade"]; //F ou M
        $nomeSing = $_POST["entidade_singular"];
        $nomePlu  = $_POST["entidade_plural"];
        $numeroCadastros = $_POST["numeroCadastros"];

        $novo[0] = "novo";
        $novo[1] = "nova";
        $novo[2] = "novos";
        $novo[3] = "novas";

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $mensagemAdicionar = "Adicionar " . $novo[$index] . " " . $nomeMsg;
        $mensagemListar = "Listar " . $nomePlu;

        $tituloFieldset = "Pesquisar " . ucwords($_POST["entidade_plural"]);

        $conteudo = "<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     $table
    * DATA DE GERA��O:    $filedate
    * ARQUIVO:            $table.php5
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */

    \$obj = new $ext();

    \$objArg = new Generic_Argument();

    \$class = \$obj->nomeClasse;
    \$action = (Helper::GET(\"id\")?\"edit\": \"add\");
    \$postar = \"index.php5\";

    \$nextActions = array(\"add_$table\"=>\"$mensagemAdicionar\",
    					 \"list_$table\"=>\"$mensagemListar\");

    if(Helper::SESSION(\"erro\")){

        unset(\$_SESSION[\"erro\"]);

       \$obj->setBySession();

    }

    \$obj->setByGet(\"1\");

    \$obj->formatarParaExibicao();

    ?>

    <?=\$obj->getCabecalhoFiltro(\$postar); ?>

    	<input type=\"hidden\" name=\"class\" id=\"class\" value=\"<?=\$class; ?>\">
        <input type=\"hidden\" name=\"tipo\" id=\"tipo\" value=\"lists\">
        <input type=\"hidden\" name=\"page\" id=\"tipo\" value=\"$table\">

        <fieldset class=\"fieldset_filtro\">
            <legend class=\"legend_filtro\">$tituloFieldset</legend>

        <table class=\"tabela_form\">

";

        $numeroColunasDb = count($arrCampos);

        if($numeroColunasDb % $colunas == 0)
            $contador = $numeroColunasDb;
        else
            $contador = $numeroColunasDb + $colunas - ($numeroColunasDb % $colunas);

        $colspan = $colunas * 2;

        for ($i=0; $i < $contador; $i++)
        {

            $col = $arrCampos[$i];
            $colUpper = ucfirst($col);

            if($i > $numeroColunasDb -1){

                $conteudo .="

            	<td class=\"td_form_label\"></td>
    			<td class=\"td_form_campo\"></td>\n";

                    if($i % $colunas == $colunas -1){

                        $conteudo .= "\t\t\t</tr>\n";

                    }

                continue;

            }

            if($i % $colunas == 0){

                $conteudo .= "\t\t\t<tr class=\"tr_form\">\n";

            }

            if(substr_count($col, "_id_") > 0){

                $tabela = substr($col, 0, strpos($col, "_id_"));
                $nomeMetodo = "All" . ucfirst($tabela);

                $conteudo .= "

    			<?

    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = false;
    			\$objArg->largura = 200;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\">
    			    <?=\$obj->getComboBox{$nomeMetodo}(\$objArg); ?>
    			</td>\n";


            }
            elseif(substr_count($col, "_BOOLEAN") > 0){

                $labelTrue = "Sim";
                $labelFalse = "N�o";

                if(substr_count($col, "status") > 0){

                   $labelTrue = "Ativo";
                   $labelFalse = "Inativo";

                }

                $conteudo .= "

    			<?

    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->labelTrue = \"$labelTrue\";
    			\$objArg->labelFalse = \"$labelFalse\";
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = false;
    			\$objArg->largura = 80;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\"><?=\$obj->imprimirCampo$colUpper(\$objArg); ?></td>\n";

            }
            else{

                $conteudo .= "

    			<?

    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = false;
    			\$objArg->largura = 200;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\"><?=\$obj->imprimirCampo$colUpper(\$objArg); ?></td>\n";


            }

            if($i % $colunas == $colunas -1){

                $conteudo .= "\t\t\t</tr>\n";

            }

        }

     $conteudo .= "
        <tr class=\"tr_form_rodape2\">
        	<td colspan=\"$colspan\">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>\n";

     $conteudo .="\t</table>

     </fieldset>

	<?=\$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=\$obj->getRodapeFormulario(); ?>

";

     fwrite($file, $conteudo);
     fclose($file);
	 
		$strGerouExt = "<p class=\"mensagem_retorno\">
            &bull;&nbsp;&nbsp;Filtro <b>$table</b> gerado com sucesso no arquivo <b>$table.php5</b>.
            </p>";

    print $strGerouExt;

    }

}

?>