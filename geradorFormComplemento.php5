<?

function gerarFormComplemento($table, $tabelaBase, $arrCampos){

    $idProjeto = $_POST["projeto"];
    
    if(!is_array($arrCampos)){
        
        echo "N�o foi poss�vel gerar o formul�rio de complemento pois nao existem configura��es salvas para a tabela ($table)";
        return;
        
    }
        
    $class = $arrCampos["classname"];
    $key = $arrCampos["keyname"];          
    $label = $arrCampos["labelname"];             
    $ext = $arrCampos["extname"];
    $sobrescrever = "1";
    
    $database = new Database();
    $objBanco = new Database();

    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioAjaxForms, p.colunasForms_INT FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));

    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);
    $colunas = Database::mysqli_result($objBanco->result, 0, 5);

    $database->OpenLink();

    $dir = dirname(__FILE__);
    $filedate = date("d.m.Y");

    $filename = $dir . "/../../" . $diretorio ."/" . "{$table}_relacionamento_{$tabelaBase}" . ".php5";

    $sql = "SHOW TABLES LIKE '$table';";
    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $file = fopen($filename, "w+");

        $genero   = $arrCampos["genero_entidade"]; //F ou M
        $nomeSing = $arrCampos["entidade_singular"];
        $nomePlu  = $arrCampos["entidade_plural"];
        $numeroCadastros = $arrCampos["numeroCadastros"];
        
        $tableUC = ucfirst($table);

        $index = 0;

        $novo[0] = "novo";
        $novo[1] = "nova";
        $novo[2] = "novos";
        $novo[3] = "novas";

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $conteudo = "<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: $table
    * DATA DE GERA��O:    $filedate
    * ARQUIVO:            $table.php5
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */
                
    if(isset(\$_GET[\"contador\"])){

        \$numeroRegistroInterno = Helper::GET(\"contador\");

    }

    if(isset(\$identificadorRelacionamento) && is_numeric(\$identificadorRelacionamento)){

        \$obj{$tableUC} = new {$ext}();
        \$obj{$tableUC}->select(\$identificadorRelacionamento);

    }
    else{

        \$obj{$tableUC} = new {$ext}();

    }

        \$objArg{$tableUC} = new Generic_Argument();
        \$obj{$tableUC}->formatarParaExibicao();

    	?>

    	<input type=\"hidden\" name=\"{$table}_{$key}_<?=\$numeroRegistroInterno ?>\" id=\"{$table}_{$key}_<?=\$numeroRegistroInterno ?>\" value=\"<?=\$obj{$tableUC}->get" . ucfirst($key) . "(); ?>\">

        <table class=\"tabela_form\">

        ";

        $sql = "SHOW COLUMNS FROM $table WHERE `Key` <> 'PRI' AND `Field` NOT LIKE '{$tabelaBase}_id_INT' AND `Field` NOT LIKE '%dataCadastro%' AND `Field` NOT LIKE '%dataEdicao%';";
        $database->query($sql);
        $result = $database->result;

        $numeroColunasDb = 0;

        while(mysqli_fetch_row($result)){

            $numeroColunasDb++;

        }

        $database->query($sql);
        $result = $database->result;

        if($numeroColunasDb % $colunas == 0)
            $contador = $numeroColunasDb;
        else
            $contador = $numeroColunasDb + $colunas - ($numeroColunasDb % $colunas);

        $colspan = $colunas * 2;

        for ($i=0; $i < $contador; $i++)
        {

            $row = mysqli_fetch_row($result);

            $col = $row[0];
            $colUpper = ucfirst($col);

            if($i > $numeroColunasDb -1){

                $conteudo .="

            	<td class=\"td_form_label\"></td>
    			<td class=\"td_form_campo\"></td>\n";

                    if($i % $colunas == $colunas -1){

                        $conteudo .= "\t\t\t</tr>\n";

                    }

                continue;

            }

            if($i % $colunas == 0){

                $conteudo .= "\t\t\t<tr class=\"tr_form\">\n";

            }

            $obrigatorio = is_array($arrCampos["camposobrigatorios"]) && in_array($col, $arrCampos["camposobrigatorios"])?"true":"false";

            if(substr_count($col, "_id_") > 0){

                $tabela = substr($col, 0, strpos($col, "_id_"));
                $nomeMetodo = "obj" . ucfirst($tabela) . "->getComboBox";

                $conteudo .= "

    			<?
                
                        \$objArg{$tableUC} = new Generic_Argument();

    			\$objArg{$tableUC}->numeroDoRegistro = \"\";
    			\$objArg{$tableUC}->label = \$obj{$tableUC}->label_{$col};
    			\$objArg{$tableUC}->valor = \$obj{$tableUC}->get$colUpper();
    			\$objArg{$tableUC}->classeCss = \"input_text\";
    			\$objArg{$tableUC}->classeCssFocus = \"focus_text\";
    			\$objArg{$tableUC}->obrigatorio = {$obrigatorio};
    			\$objArg{$tableUC}->largura = 200;
                        \$objArg{$tableUC}->nome = \"{$table}_{$col}_{\$numeroRegistroInterno}\";
                        \$objArg{$tableUC}->id = \"{$table}_{$col}_{\$numeroRegistroInterno}\";

    			\$obj{$tableUC}->addInfoCampos(\"$col\", \$objArg{$tableUC}->label, \"TEXTO\", \$objArg{$tableUC}->obrigatorio);

    			?>

    			<td class=\"td_form_label\"><?=\$objArg{$tableUC}->getLabel() ?></td>
    			<td class=\"td_form_campo\">
    			    <?=\$obj{$tableUC}->{$nomeMetodo}(\$objArg{$tableUC}); ?>
    			</td>\n

                    ";


        }
        elseif(substr_count($col, "_BOOLEAN") > 0){

            $labelTrue = "Sim";
            $labelFalse = "N�o";

            if(substr_count($col, "status") > 0){

               $labelTrue = "Ativo";
               $labelFalse = "Inativo";

            }

            $conteudo .= "

                    <?

                    \$objArg{$tableUC} = new Generic_Argument();

                    \$objArg{$tableUC}->numeroDoRegistro = \"\";
                    \$objArg{$tableUC}->label = \$obj{$tableUC}->label_{$col};
                    \$objArg{$tableUC}->labelTrue = \"{$labelTrue}\";
                    \$objArg{$tableUC}->labelFalse = \"{$labelFalse}\";
                    \$objArg{$tableUC}->valor = \$id?\$obj{$tableUC}->get$colUpper():1;
                    \$objArg{$tableUC}->classeCss = \"input_text\";
                    \$objArg{$tableUC}->classeCssFocus = \"focus_text\";
                    \$objArg{$tableUC}->obrigatorio = {$obrigatorio};
                    \$objArg{$tableUC}->largura = 20;
                    \$objArg{$tableUC}->nome = \"{$table}_{$col}_{\$numeroRegistroInterno}\";
                    \$objArg{$tableUC}->id = \"{$table}_{$col}_{\$numeroRegistroInterno}\";

                    ?>

                    <td class=\"td_form_label\"><?=\$objArg{$tableUC}->getLabel() ?></td>
                    <td class=\"td_form_campo\"><?=\$obj{$tableUC}->campoBoolean(\$objArg{$tableUC}); ?></td>\n";

        }
        elseif(substr_count($col, "_HTML") > 0){

            $conteudo .= "

                    <?

                    \$objArg{$tableUC} = new Generic_Argument();

                    \$objArg{$tableUC}->label = \$obj{$tableUC}->label_{$col};
                    \$objArg{$tableUC}->valor = \$obj{$tableUC}->get$colUpper();
                    \$objArg{$tableUC}->obrigatorio = {$obrigatorio};
                    \$objArg{$tableUC}->largura = \"98%\";
                    \$objArg{$tableUC}->altura = 330;
                    \$objArg{$tableUC}->nome = \"{$table}_{$col}_{\$numeroRegistroInterno}\";
                    \$objArg{$tableUC}->id = \"{$table}_{$col}_{\$numeroRegistroInterno}\";




                    ?>

                    <td class=\"td_form_label\"><?=\$objArg{$tableUC}->getLabel() ?></td>
                    <td class=\"td_form_campo\">

                    <?

                    Helper::includePHP(1, \"recursos/libs/fckeditor/fckeditor.php5\");

                    \$editor = new FCKeditor(\$objArg{$tableUC}->nome); //Nomeia a �rea de texto
                    \$editor-> BasePath = \"../recursos/libs/fckeditor/\";  //Informa a pasta do FKC Editor
                    \$editor-> ToolbarSet = \"Basic\";
                    \$editor-> Value = html_entity_decode(\$objArg{$tableUC}->valor); //Informa o valor inicial do campo, no exemplo est� vazio
                    \$editor-> Width = \$objArg{$tableUC}->largura;          //informa a largura do editor
                    \$editor-> Height = \$objArg{$tableUC}->altura;         //informa a altura do editor
                    \$editor-> Create();               // Cria o editor

            ?>

                    </td>\n";

        }            
        else{

            if(substr_count($col, "_DATE") > 0){

                $nomeMetodo = "campoData";

            }
            elseif(substr_count($col, "_DATETIME") > 0){

                $nomeMetodo = "campoDataHora";

            }
            elseif(substr_count($col, "_TIME") > 0){

                $nomeMetodo = "campoHora";

            }
            elseif(substr_count($col, "_FLOAT") > 0){

                $nomeMetodo = "campoMoeda";

            }
            elseif(substr_count($col, "_INT") > 0){

                $nomeMetodo = "campoInteiro";

            }
            elseif(substr_count($col, "_IMAGEM") > 0){

                $nomeMetodo = "campoImagem";

            }
            elseif(substr_count($col, "_ARQUIVO") > 0){

                $nomeMetodo = "campoArquivo";

            }
            elseif(substr_count($col, "cpf") > 0){

                $nomeMetodo = "campoCPF";

            }
            elseif(substr_count($col, "cnpj") > 0){

                $nomeMetodo = "campoCNPJ";

            }
            elseif(substr_count($col, "cep") > 0){

                $nomeMetodo = "campoCep";

            }
            elseif(substr_count($col, "email") > 0){

                $nomeMetodo = "campoEmail";

            }
            elseif(substr_count($col, "telefone") > 0 || substr_count($col, "celular") > 0){

                $nomeMetodo = "campoTelefone";

            }
            else{

                $nomeMetodo = "campoTexto";

            }


            $conteudo .= "

                    <?

                    \$objArg{$tableUC} = new Generic_Argument();

                    \$objArg{$tableUC}->numeroDoRegistro = \"\";
                    \$objArg{$tableUC}->label = \$obj{$tableUC}->label_{$col};
                    \$objArg{$tableUC}->valor = \$obj{$tableUC}->get$colUpper();
                    \$objArg{$tableUC}->classeCss = \"input_text\";
                    \$objArg{$tableUC}->classeCssFocus = \"focus_text\";
                    \$objArg{$tableUC}->obrigatorio = {$obrigatorio};
                    \$objArg{$tableUC}->largura = 200;
                    \$objArg{$tableUC}->nome = \"{$table}_{$col}_{\$numeroRegistroInterno}\";
                    \$objArg{$tableUC}->id = \"{$table}_{$col}_{\$numeroRegistroInterno}\";

                    ?>

                    <td class=\"td_form_label\"><?=\$objArg{$tableUC}->getLabel() ?></td>
                    <td class=\"td_form_campo\"><?=\$obj{$tableUC}->{$nomeMetodo}(\$objArg{$tableUC}); ?></td>\n";

        }

        if($i % $colunas == $colunas -1){

            $conteudo .= "\t\t\t</tr>\n";

        }

    }
    
    $nomeBotaoRemover = ucwords($nomeSing);
    
    $conteudo .= "\t<tr><td colspan=\"{$colspan}\" class=\"td_botao_remover_da_lista\"><input class=\"botoes_form\" type=\"button\" value=\"Remover {$nomeBotaoRemover}\" onclick=\"javascript:removerDivAjaxEmLista(this);\"></td></tr>";

    $conteudo .="\t</table><br />

";

     fwrite($file, $conteudo);
     fclose($file);

        $strGerouExt = "<p class=\"mensagem_retorno\">
            &bull;&nbsp;&nbsp;Formul�rio de suporte <b>$table</b> gerado com sucesso no arquivo <b>{$table}_complemento.php5</b>.
            </p>";

    print $strGerouExt;

    }

}

?>