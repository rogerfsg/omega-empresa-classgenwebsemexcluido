<?

function gerarFormMultiplo($table, $class, $key, $label, $ext, $sobrescrever, $numeroRegs){

    $tabelasRelacionamento = $_POST["tabelasrelacionamento"];
    
    $idProjeto = $_POST["projeto"];
    
    foreach($tabelasRelacionamento as $tabelaRelacionamento){
        
        $databaseAux = new Database();
        $databaseAux->query("SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT={$idProjeto} AND tabela_nome='{$tabelaRelacionamento}'");

        while ($row = mysqli_fetch_array($databaseAux->result)){

            $arrCamposComplemento = unserialize(html_entity_decode($row[0]));

        }
        
        gerarFormComplemento($tabelaRelacionamento, $table, $arrCamposComplemento);
        
    }
        
    $database = new Database();
    $objBanco = new Database();

    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioForms, p.colunasForms_INT FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));

    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);
    $colunas = Database::mysqli_result($objBanco->result, 0, 5);

    $database->OpenLink();

    $dir = dirname(__FILE__);
    $filedate = date("d.m.Y");

    $filename = $dir . "/../../" . $diretorio ."/" . $table . ".php5";

    $sql = "SHOW TABLES LIKE '$table';";
    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $file = fopen($filename, "w+");

        $genero   = $_POST["genero_entidade"]; //F ou M
        $nomeSing = $_POST["entidade_singular"];
        $nomePlu  = $_POST["entidade_plural"];
        $numeroCadastros = $_POST["numeroCadastros"];

        $index = 0;

        $novo[0] = "novo";
        $novo[1] = "nova";
        $novo[2] = "novos";
        $novo[3] = "novas";

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $mensagemAdicionar = "Adicionar " . $novo[$index] . " " . $nomeMsg;
        $mensagemListar = "Listar " . $nomePlu;

        $conteudo = "<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: $table
    * DATA DE GERA��O:    $filedate
    * ARQUIVO:            $table.php5
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */

    \$obj = new $ext();

    \$objArg = new Generic_Argument();

    \$numeroRegistros = $numeroRegs;
    \$class = \$obj->nomeClasse;
    \$action = (Helper::GET(\"id1\")?\"edit\": \"add\");
    \$postar = \"actions.php5\";

    \$nextActions = array(\"add_$table\"=>\"$mensagemAdicionar\",
    					 \"list_$table\"=>\"$mensagemListar\");

    ?>

    <?=\$obj->getCabecalhoFormulario(\$postar); ?>

        <input type=\"hidden\" name=\"numeroRegs\" id=\"numeroRegs\" value=\"<?=\$numeroRegistros; ?>\">
    	<input type=\"hidden\" name=\"class\" id=\"class\" value=\"<?=\$class; ?>\">
        <input type=\"hidden\" name=\"action\" id=\"action\" value=\"<?=\$action; ?>\">
    	<input type=\"hidden\" name=\"origin_action\" id=\"origin_action\" value=\"<?=\$action; ?>_{$table}\">

    	<?

    	for(\$cont=1; \$cont <= \$numeroRegistros; \$cont++){

            if(Helper::SESSION(\"erro\")){

                unset(\$_SESSION[\"erro\"]);

               \$obj->setBySession();

            }

            if(Helper::GET(\"id{\$cont}\")){

                \$id = Helper::GET(\"id{\$cont}\");

                \$obj->select(\$id);
                \$legend = \"Atualizar {$nomeSing}\";

            }
            else{

            	\$legend = \"Cadastrar {$nomeSing}\";

            }

            \$obj->formatarParaExibicao();

    	?>

    	<input type=\"hidden\" name=\"$key<?=\$cont ?>\" id=\"$key<?=\$cont ?>\" value=\"<?=\$obj->get" . ucfirst($key) . "(); ?>\">

    	<fieldset class=\"fieldset_form\">
            <legend class=\"legend_form\"><?=\$legend; ?></legend>

        <table class=\"tabela_form\">

        ";

        $sql = "SHOW COLUMNS FROM $table WHERE `Key` <> 'PRI' AND `Field` NOT LIKE '%dataCadastro%' AND `Field` NOT LIKE '%dataEdicao%';";
        $database->query($sql);
        $result = $database->result;

        $numeroColunasDb = 0;

        while(mysqli_fetch_row($result)){

            $numeroColunasDb++;

        }
        
        $camposHidden = $_POST["camposformulariohidden"];
        
        $numeroCamposHidden = count($camposHidden);        
                
        for($i=0; $i < $numeroCamposHidden; $i++){
            
            $colUpper = ucfirst($camposHidden[$i]);
            
            $conteudo .="\t<input type=\"hidden\" name=\"{$camposHidden[$i]}\" value=\"<?=\$obj->get{$colUpper}(); ?>\" />\n";
            
        } 

        $database->query($sql);
        $result = $database->result;

        if($numeroColunasDb % $colunas == 0)
            $contador = $numeroColunasDb;
        else
            $contador = $numeroColunasDb + $colunas - ($numeroColunasDb % $colunas);

        $colspan = $colunas * 2;

        for ($i=0; $i < $contador; $i++)
        {

            $row = mysqli_fetch_row($result);
            
            if($row === false){
                
                break;
                
            }
            
            $col = $row[0];
            $colUpper = ucfirst($col);
            
            if(in_array($col, $camposHidden)){
                
                continue;
                
            }   
            
            if($i > $numeroColunasDb -1){

                $conteudo .="

            	<td class=\"td_form_label\"></td>
    			<td class=\"td_form_campo\"></td>\n";

                    if($i % $colunas == $colunas -1){

                        $conteudo .= "\t\t\t</tr>\n";

                    }

                continue;

            }

            if($i % $colunas == 0){

                $conteudo .= "\t\t\t<tr class=\"tr_form\">\n";

            }

            $obrigatorio = is_array($_POST["camposobrigatorios"]) && in_array($col, $_POST["camposobrigatorios"])?"true":"false";

            if(substr_count($col, "_id_") > 0){

                $tabela = substr($col, 0, strpos($col, "_id_"));
                $nomeMetodo = "All" . ucfirst($tabela);

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = 200;

    			\$obj->addInfoCampos(\"$col\", \$objArg->label, \"TEXTO\", \$objArg->obrigatorio);

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\">
    			    <?=\$obj->getComboBox{$nomeMetodo}(\$objArg); ?>
    			</td>\n

    			";


            }
            elseif(substr_count($col, "_BOOLEAN") > 0){

                $labelTrue = "Sim";
                $labelFalse = "N�o";

                if(substr_count($col, "status") > 0){

                   $labelTrue = "Ativo";
                   $labelFalse = "Inativo";

                }

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->labelTrue = \"{$labelTrue}\";
    			\$objArg->labelFalse = \"{$labelFalse}\";
    			\$objArg->valor = \$obj->get{$colUpper}();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = 80;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\"><?=\$obj->imprimirCampo{$colUpper}(\$objArg); ?></td>\n";

            }
            elseif(substr_count($col, "_HTML") > 0){

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = \"98%\";
    			\$objArg->altura = 330;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\">

    			<?

    			Helper::includePHP(1, \"recursos/libs/fckeditor/fckeditor.php5\");

    			\$editor = new FCKeditor(\"$col\" . \$objArg->numeroDoRegistro); //Nomeia a �rea de texto
                        \$editor-> BasePath = \"../recursos/libs/fckeditor/\";  //Informa a pasta do FKC Editor
                        \$editor-> ToolbarSet = \"Basic\";
                        \$editor-> Value = html_entity_decode(\$objArg->valor); //Informa o valor inicial do campo, no exemplo est� vazio
                        \$editor-> Width = \$objArg->largura;          //informa a largura do editor
                        \$editor-> Height = \$objArg->altura;         //informa a altura do editor
                        \$editor-> Create();               // Cria o editor

                        ?>

    			</td>\n";


            }
            elseif(substr_count($col, "_TEXTO") > 0){

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = \"98%\";
    			\$objArg->altura = 200;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\">

                            <textarea id=\"$col<?=\$objArg->numeroDoRegistro ?>\" name=\"$col<?=\$objArg->numeroDoRegistro ?>\" style=\"height:<?=\$objArg->altura ?>;width:<?=\$objArg->largura ?>\"><?=\$objArg->valor ?></textarea>

    			</td>\n";


            }
            else{

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCssFocus = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = 200;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\"><?=\$obj->imprimirCampo$colUpper(\$objArg); ?></td>\n";


            }

            if($i % $colunas == $colunas -1){

                $conteudo .= "\t\t\t</tr>\n";

            }

        }


     $conteudo .= "

     	 <? } ?>

         <tr class=\"tr_form_rodape1\">
            <td colspan=\"$colspan\">

                <?=Helper::getBarraDaNextAction(\$nextActions); ?>

            </td>
        </tr>
        <tr class=\"tr_form_rodape2\">
            <td colspan=\"$colspan\" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, \$action==\"edit\"?true:false); ?>

            </td>
        </tr>\n";

     $conteudo .="\t</table>

     </fieldset>";
     
     foreach($tabelasRelacionamento as $tabelaRelacionamento){
        
        $databaseAux = new Database();
        $databaseAux->query("SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT={$idProjeto} AND tabela_nome='{$tabelaRelacionamento}'");

        while ($row = mysqli_fetch_array($databaseAux->result)){

            $arrCamposComplemento = unserialize(html_entity_decode($row[0]));

        }
    
        $nomeEntidade = ucwords($arrCamposComplemento["entidade_singular"]);
        $nomeEntidadePlural = ucwords($arrCamposComplemento["entidade_plural"]);
       
     $conteudo .= "     

         <fieldset class=\"fieldset_form\">
            <legend class=\"legend_form\">{$nomeEntidadePlural}</legend>

            <?
            
            \$numeroRegistroInterno = 1;
            
            if(is_numeric(\$id)){

                \$objBanco->query(\"SELECT id FROM {$arrCamposComplemento["tablename"]}
                                    WHERE {$table}_id_INT={\$id} ORDER BY id\");

                for(\$numeroRegistroInterno=1; \$dados = \$objBanco->fetchArray(); \$numeroRegistroInterno++){

                    \$identificadorRelacionamento = \$dados[0];

                    echo \"<div class=\\\"container_da_lista\\\" contador=\\\"{\$numeroRegistroInterno}\\\">\";
                                    
                        include('ajax_forms/{$arrCamposComplemento["tablename"]}_relacionamento_{$table}.php5');
                    
                    echo \"</div>\";
                    
                    unset(\$identificadorRelacionamento);

                }
                                
            }
            
            //\$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista(\"{$arrCamposComplemento["tablename"]}\", \$numeroRegistroInterno); ?>

                <table class=\"tabela_form\">

                    <tr class=\"tr_form\">

                        <td class=\"td_botao_adicionar_novo_bloco\">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista(\"ajax_forms\", \"{$arrCamposComplemento["tablename"]}_relacionamento_{$table}\", \"div#{$arrCamposComplemento["tablename"]}\", \"Adicionar {$nomeEntidade}\") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />";
     
     }
     
     $conteudo .= "

	<?=\$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=\$obj->getRodapeFormulario(); ?>

";

     fwrite($file, $conteudo);
     fclose($file);

        $strGerouExt = "<p class=\"mensagem_retorno\">
            &bull;&nbsp;&nbsp;Formul�rio <b>$table</b> gerado com sucesso no arquivo <b>$table.php5</b>.
            </p>";

    print $strGerouExt;

    }

}

?>